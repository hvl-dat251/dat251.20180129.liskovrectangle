package dat251.liskovrectangle.problem;

public class Square extends Rectangle {

	//Liskov-violation!!!
	@Override
	public void setHeight(double height) {
		super.setHeight(height);
		super.setWidth(height);
	}

	//Liskov-violation!!!
	@Override
	public void setWidth(double width) {
		super.setHeight(width);
		super.setWidth(width);
	}

}
