package dat251.liskovrectangle.problem;

public class Rectangle implements TwoDimensionalShape {
	
	private double height;
	private double width;

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	@Override
	public double area() {
		return height * width;
	}

	@Override
	public double perimeter() {
		return 2* height + 2* width;
	}

}
