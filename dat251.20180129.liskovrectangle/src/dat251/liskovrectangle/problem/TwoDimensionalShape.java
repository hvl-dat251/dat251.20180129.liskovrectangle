package dat251.liskovrectangle.problem;

public interface TwoDimensionalShape {
	
	double area();
	double perimeter();

}
