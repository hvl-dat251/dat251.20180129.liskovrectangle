package dat251.liskovrectangle.problem;

public class Main {

	public static void main(String[] args) {
		
		Rectangle s1 = new Rectangle();
		s1.setHeight(3);
		s1.setWidth(4);
		
		System.out.println("H: " + s1.getHeight());
		System.out.println("W: " + s1.getWidth());
		System.out.println("A: " + s1.area());
		System.out.println("P: " + s1.perimeter());

		Square s2 = new Square();
		s2.setHeight(3);
		s2.setWidth(4); //What are we doing here? Constraint violation?
		
		System.out.println("H: " + s2.getHeight());
		System.out.println("W: " + s2.getWidth());
		System.out.println("A: " + s2.area());
		System.out.println("P: " + s2.perimeter());
	}

}
